(function ($, Drupal, window, document, undefined) {

  'use strict';

  Drupal.behaviors.fileDisableAutoUpload = {
    attach: function (context) {
      var $context = $(context);

      $context.find('input[type="file"]').removeOnce('auto-file-upload').off('.autoFileUpload', Drupal.file.triggerUploadButton);
      $context.find('.js-form-submit').off('mousedown', Drupal.file.disableFields);
    }
  };

})(jQuery, Drupal, this, this.document);

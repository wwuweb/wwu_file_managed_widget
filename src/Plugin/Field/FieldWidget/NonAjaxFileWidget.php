<?php

namespace Drupal\wwu_file_managed_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;
use Drupal\file\Element\ManagedFile;

class NonAjaxFileWidget extends FileWidget {

  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  public static function process($element, FormStateInterface $form_state, $form) {
    $result = parent::process($element, $form_state, $form);

    unset($result['upload_button']);

    $result['#progress_indicator'] = 'none';
    $result['remove_button']['#ajax']['progress']['type'] = 'none';

    if (empty($result['#files'])) {
      unset($result['#prefix']);
      unset($result['#suffix']);
      unset($result['remove_button']);
    }

    return $result;
  }

}
